from django.contrib import admin

from price.models import Batterys, Wheels, Tyres

class BatterysAdmin(admin.ModelAdmin):
    list_display = ('name',  'capacity', 'size', 'polarity', 'starting_current', 'manufacturer', 'price',)

admin.site.register(Batterys, BatterysAdmin)
admin.site.register(Wheels)
admin.site.register(Tyres)