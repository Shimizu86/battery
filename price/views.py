from django.core import serializers
from django.db.models.loading import get_model
from django.http import JsonResponse, HttpResponseServerError, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.generic import CreateView

from contact.models import Contact
from price.forms import TyresForm, BatteriesForm, WheelsForm
from price.models import Batterys, Tyres, Wheels
from slider.models import Slider


def home(request):
    context = {"slider": Slider.objects.all(),
               'contacts': Contact.objects.all()}
    return render(request, "home.html", context)


def info(request):
    context = {"slider": Slider.objects.all(),}
    return render(request, "info.html", context)


class BaseView(CreateView):
    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)
        context['slider'] = Slider.objects.all()
        queryset = self.model.objects.all()
        context['goods'] = self.filter_queryset(queryset)
        context['contacts'] = Contact.objects.all()
        return context

    def filter_queryset(self, queryset):
        filter_set = {key: value for key, value in self.request.GET._iteritems() if value}
        return queryset.filter(**filter_set)


class BatterysView(BaseView):
    model = Batterys
    form_class = BatteriesForm


class WheelsView(BaseView):
    model = Wheels
    form_class = WheelsForm


class TyresView(BaseView):
    model = Tyres
    form_class = TyresForm


def goods_filter(request):
    if request.is_ajax():
        filter_dict = request.GET.copy()
        model = get_model('price', filter_dict['model'])
        template = filter_dict['template']
        filter_dict.pop('model')
        filter_dict.pop('template')
        filter_set = {item: filter_dict[item] for item in filter_dict if filter_dict[item]}
        filtered_goods = model.objects.filter(**filter_set)
        html = render_to_string('template_parts/' + template, {'goods': filtered_goods})
        return HttpResponse(html)
        # return JsonResponse({'goods': serializers.serialize('json', filtered_goods)})
    else:
        raise HttpResponseServerError
