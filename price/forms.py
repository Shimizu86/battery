from django import forms

from price.models import Tyres, Wheels, Batterys


class TyresForm(forms.ModelForm):

    class Meta(object):
        model = Tyres
        fields = ('season', 'width', 'profile', 'diameter', 'manufacturer', )


class BatteriesForm(forms.ModelForm):

    class Meta(object):
        model = Batterys
        fields = ('capacity', 'manufacturer', 'polarity', )


class WheelsForm(forms.ModelForm):

    class Meta(object):
        model = Wheels
        fields = ('manufacturer', 'diameter', 'offset', 'pcd', 'width', 'DIA', )
