from django.conf.urls import url

from price.views import BatterysView, WheelsView, TyresView, goods_filter, info

urlpatterns = [
    url(r'^batterys/icons/$', BatterysView.as_view(template_name='batteries_as_icons.html'), name='battery_icons'),
    url(r'^batterys/table/$', BatterysView.as_view(template_name= 'batteries_as_table.html'), name='battery_table'),
    url(r'^wheels/icons/$', WheelsView.as_view(template_name='wheels_as_icons.html'), name='wheels_icons'),
    url(r'^wheels/table/$', WheelsView.as_view(template_name='wheels_as_table.html'), name='wheels_table'),
    url(r'^tyres/icons/$', TyresView.as_view(template_name='tyres_as_icons.html'), name='tyres_icons'),
    url(r'^tyres/table/$', TyresView.as_view(template_name='tyres_as_table.html'), name='tyres_table'),
    url(r'^filter/', goods_filter, name='goods_filter'),
    url(r'^info/', info, name='info'),
]

