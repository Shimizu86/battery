from PIL import Image, ImageOps
from django.db import models
from .model_choices import *


class Batterys(models.Model):
    name = models.CharField(max_length=200, verbose_name='Наименование')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    picture = models.ImageField(upload_to='goods/batteries', verbose_name='Изображение')
    size = models.CharField(max_length=30, verbose_name='Размеры, мм', default='0x0x0')
    manufacturer = models.CharField(max_length=200, verbose_name='Производитель', choices=BATTERY_MANUFACTURER_CHOICES)
    capacity = models.IntegerField(verbose_name='Емкость', choices=BATTERY_CAPACITY_CHOICES)
    polarity = models.CharField(max_length=200, verbose_name='Полярность', choices=BATTERY_POLARITY_CHOICES)
    starting_current = models.IntegerField(verbose_name='Пусковой ток', default=0)

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Batterys, self).delete(*args, **kwargs)
        storage.delete(path)

    def save(self, *args, **kwargs):
        try:
            slide = Batterys.objects.get(id=self.id)
            if slide.image != self.image:
                slide.image.delete()
        except: pass

        super(Batterys, self).save(*args, **kwargs)

        img = Image.open(self.picture.path)
        img = ImageOps.fit(img, (640, 480), Image.ANTIALIAS)
        img.save(self.picture.path, quality=100)


class Wheels(models.Model):
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    picture = models.ImageField(upload_to='goods/wheels')
    manufacturer = models.CharField(max_length=200, verbose_name='Производитель', choices=WHEELS_MANUFACTURER)
    diameter = models.IntegerField(verbose_name='Диаметр', choices=WHEELS_DAIMETER)
    offset = models.CharField(max_length=200, verbose_name='Вылет(ET)', choices=WHEELS_OFFSET)
    pcd = models.CharField(max_length=200, verbose_name='PCD', choices=WHEELS_PCD)
    width = models.CharField(max_length=10, verbose_name='Ширина', choices=WHEELS_WIDTH)
    DIA = models.CharField(max_length=200, verbose_name='DIA', choices=WHEELS_DIA)
    model = models.CharField(max_length=200, verbose_name='Модель')

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Wheels, self).delete(*args, **kwargs)
        storage.delete(path)

    def save(self, *args, **kwargs):
        try:
            slide = Wheels.objects.get(id=self.id)
            if slide.image != self.image:
                slide.image.delete()
        except: pass

        super(Wheels, self).save(*args, **kwargs)

        img = Image.open(self.picture.path)
        img = ImageOps.fit(img, (640, 480), Image.ANTIALIAS)
        img.save(self.picture.path, quality=100)


class Tyres(models.Model):
    name = models.CharField(max_length=200, verbose_name='Наименование')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    picture = models.ImageField(upload_to='goods/tyres', verbose_name='Изображение')
    season = models.CharField(max_length=200, verbose_name='Сезон', choices=TYRES_SEASON_CHOICES)
    model = models.CharField(max_length=200, verbose_name='Модель')
    manufacturer = models.CharField(max_length=200, verbose_name='Производитель', choices=TYRES_MANUFACTURER)
    diameter = models.CharField(max_length=10, verbose_name='Диаметр', choices=TYRES_DIAMETER)
    width = models.CharField(max_length=200, verbose_name='Ширина', choices=TYRES_WIDTH)
    profile = models.CharField(max_length=200, verbose_name='Профиль', choices=TYRES_PROFILE)
    speed_index = models.CharField(max_length=10, verbose_name='Скоростной индекс')

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Tyres, self).delete(*args, **kwargs)
        storage.delete(path)

    def save(self, *args, **kwargs):
        try:
            slide = Tyres.objects.get(id=self.id)
            if slide.image != self.image:
                slide.image.delete()
        except: pass

        super(Tyres, self).save(*args, **kwargs)

        img = Image.open(self.picture.path)
        img = ImageOps.fit(img, (640, 480), Image.ANTIALIAS)
        img.save(self.picture.path, quality=100)
