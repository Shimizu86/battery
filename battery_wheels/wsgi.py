import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "battery_wheels.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
