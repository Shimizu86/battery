$(document).ready(function () {
    var content = $('#form'),
        path = $(location).attr('pathname'),
        model = path.split('/')[2],
        view = path.split('/')[3];
    content.change(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/price/filter/',
            type: 'GET',
            data: content.serialize() + '&model=' + model + '&template=' + model + '_' + view + '.html',
            success: function(data){
                $(".goods").empty().html(data)
            },
            error: function (xhr) {
                console.log("Bad");
            }
        })
    });
});
