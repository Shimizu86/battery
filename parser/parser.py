# coding: utf8

from xlrd import open_workbook
import simplejson as json
import re
import os

json_folder = '/var/www/battery_wheels/price/fixtures/'
files = os.listdir('files')
dict_list = []
pk=1

for file in files:
    book = open_workbook('files/' + file)
    sheet = book.sheet_by_index(0)

    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    print("keys are", keys)
    for row_index in range(1, sheet.nrows):
        d = {keys[col_index]: sheet.cell(row_index, col_index).value
             for col_index in range(sheet.ncols)}
        parsed_name = [value for value in d['name'].split() if value != ""]
        capacity_val = parsed_name[0]
        d['polarity'] = 'Рос. +/-' if capacity_val[-1].isdigit() else 'Евр. -/+'
        d['capacity'] = int(capacity_val) if capacity_val.isdigit()  else int(capacity_val[:-1])
        d['name'] = re.sub(r'\s+', ' ', d['name'])
        if d['name'][0] == " ":
            d['name'] = d['name'][1:]

        d["picture"] = "main/1.jpg"
        dict_list.append({"model": "price.battery", "fields": d, 'pk':pk})
        pk += 1

j = json.dumps(dict_list)

with open(json_folder + 'Goods.json', 'w') as f:
    f.write(j)
