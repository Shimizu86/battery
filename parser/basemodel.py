
from xlrd import open_workbook
import simplejson as json
import re


dict_list = []
for i in range(1, 1000):
    d={}
    d["fields"] = {}
    d['model'] = "price.basepricemodel"
    d['pk'] = i
    dict_list.append(d)

#print dict_list
j = json.dumps(dict_list)

# Write to file
with open('base.json', 'w') as f:
    f.write(j)
