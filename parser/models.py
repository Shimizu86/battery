import os

import re
from django.db import models
from xlrd import open_workbook

from django.db.models.loading import get_model

from price.models import Batterys, Tyres


class DataFile(models.Model):
    file = models.FileField(verbose_name='Файл прайса', upload_to='price')

    def __str__(self):
        return self.file.url

    class Meta:
        verbose_name = "Файл загрузки"
        verbose_name_plural = 'Файлы загрузки'

    def delete(self, *args, **kwargs):
        storage, path = self.file.storage, self.file.path
        super(DataFile, self).delete(*args, **kwargs)
        storage.delete(path)

    def save(self, *args, **kwargs):
        super(DataFile, self).save(*args, **kwargs)
        try:
            file_for_parse = open_workbook(self.file.path)
            model_str = os.path.basename(self.file.path).split('.')[0]
            self.delete()
            model = get_model('price', model_str)
            model.objects.all().delete()
            sheet = file_for_parse.sheet_by_index(0)
            keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
            for row_index in range(1, sheet.nrows):
                d = {keys[col_index]: sheet.cell(row_index, col_index).value for col_index in range(sheet.ncols)}
                parsed_name = [value for value in d['name'].split() if value != ""]
                capacity_val = parsed_name[0]
                d['polarity'] = 'Рос. +/-' if capacity_val[-1].isdigit() else 'Евр. -/+'
                d['capacity'] = int(capacity_val) if capacity_val.isdigit()  else int(capacity_val[:-1])
                d['name'] = re.sub(r'\s+', ' ', d['name'])
                if d['name'][0] == " ":
                    d['name'] = d['name'][1:]
                d["picture"] = "main/1.jpg"
                Batterys.objects.create(**d)
        except:
            pass