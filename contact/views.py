from django.http import JsonResponse
from django.views.generic import FormView

from contact.models import Contact
from slider.models import Slider
from .forms import ContactForm


class ContactView(FormView):
    template_name = 'contact.html'
    form_class = ContactForm
    success_url = '#'

    def form_valid(self, form):
        form.send_mail()
        return JsonResponse({'status': 'Ok',})

    def form_invalid(self, form):
        return JsonResponse({'status': 'error', 'errors': form.errors})

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['slider'] = Slider.objects.all()
        context['contacts'] = Contact.objects.all()
        return context
