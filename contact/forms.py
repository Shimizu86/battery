from django import forms
import re
from django.core.mail import send_mail
from battery_wheels.settings import FEEDBACK_RECIPIENTS


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))
    sender = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def send_mail(self):
        send_mail(
            self.cleaned_data['subject'],
            self.cleaned_data['sender'] + ' прислал вам сообщение на чистик-спб: ' + self.cleaned_data['message'],
            self.cleaned_data['sender'],
            FEEDBACK_RECIPIENTS
        )
